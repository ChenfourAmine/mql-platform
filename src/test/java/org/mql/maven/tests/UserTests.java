package org.mql.maven.tests;


import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mql.maven.models.User;


//Senario de tests
public class UserTests {

	@BeforeAll
	public static void testGetUsername() {
		System.out.println("@BeforeAll");
		User u = new User("Amine Chenfour", "123"); 
		
		assertEquals(u.getUsername(), "Amine Chenfour");
	}
	
	@BeforeEach
	public void testGetPassword() {
		System.out.println("@BeforeEach");
		User u = new User("Karim Bouskri", "123"); 
		
		assertTrue(u.getUsername().equals("Karim Bouskri"));
	}
	
	@Test
	public void testSetters() {
		System.out.println("@Test");
		User u = new User();
		u.setUsername("Sanaa Daoudi");
		u.setPassword("123");
		
		assertAll(
			() -> assertEquals(u.getUsername(), "Sanaa Daoudi"),
			() -> assertEquals(u.getPassword(), "123")
		);
	}
	
	@AfterAll
	public static void testSame() {
		System.out.println("@AfterAll");
		User u1 = new User("Loubna Aouragh", "123"); 
		User u2 = u1; 
		
		assertSame(u1, u2);
	}

}
