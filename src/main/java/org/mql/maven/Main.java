package org.mql.maven;

import org.mql.maven.models.User;

import com.google.gson.Gson;

public class Main {

	public Main() {
		exp1();
	}
	
	public void exp1() {
		User user = new User("Amine Chenfour", "123");
		Gson gson = new Gson();
		String json = gson.toJson(user);
		System.out.println(json);
	}
	
	public static void main(String[] args) {
		new Main();
	}

}
